import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import * as strings from 'NanFungNewsWebPartStrings';
import NanFungNews from './components/NanFungNews';
import { INanFungNewsProps } from './components/INanFungNewsProps';

export interface INanFungNewsWebPartProps {
  description: string;
}

export default class NanFungNewsWebPart extends BaseClientSideWebPart<INanFungNewsWebPartProps> {

  public render(): void {

    const element: React.ReactElement<INanFungNewsProps> = React.createElement(
      NanFungNews,
      {
        description: this.context.sdks.microsoftTeams.context.entityId
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
