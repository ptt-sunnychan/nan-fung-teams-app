declare interface INanFungNewsWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'NanFungNewsWebPartStrings' {
  const strings: INanFungNewsWebPartStrings;
  export = strings;
}
