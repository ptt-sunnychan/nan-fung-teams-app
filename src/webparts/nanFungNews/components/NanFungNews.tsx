import * as React from "react";
import styles from "./NanFungNews.module.scss";
import { INanFungNewsProps } from "./INanFungNewsProps";
import { escape } from "@microsoft/sp-lodash-subset";
import * as microsoftTeams from "@microsoft/teams-js";

export default class NanFungNews extends React.Component<
  INanFungNewsProps,
  { state1: any; location: any }
> {
  constructor(props) {
    super(props);
    this.state = {
      state1: null,
      location: "home",
    };
    this.changeLocation = this.changeLocation.bind(this);
  }
  public componentDidMount() {
    microsoftTeams.initialize(() => {
      microsoftTeams.getContext((c) => {
        this.setState({ state1: c, location: c.subEntityId });
      });
    });
  }

  public changeLocation() {
    if (this.state.location == "home") {
      this.setState({ location: "detail" });
    } else {
      this.setState({ location: "home" });
    }
  }

  public render(): React.ReactElement<INanFungNewsProps> {
    return (
      <div>
        <a onClick={this.changeLocation}>Home</a>
        <a href="https://www.nanfung.com/en/" target="_blank">
          External Links
        </a>
        <iframe
          src={
            this.state.location != "detail"
              ? "https://aa-orgchart-dev.azurewebsites.net"
              : "https://pttlab.sharepoint.com/sites/CLP/SitePages/Nan-Fung-Group%E2%80%99s-Flagship-Project-%E2%80%9CAIRSIDE%E2%80%9D-topped-out-in-Kai-Tak.-A-HK$32-billion-project-that-will-reveal-Ho.aspx"
          }
          title="Nan Fung Teams App"
          width="100%"
          height="800px"
        ></iframe>
      </div>
    );
  }
}
